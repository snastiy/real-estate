package service;

import access.RealtorDao;
import access.impl.RealtorDaoImpl;
import model.Realtor;

import java.util.InputMismatchException;
import java.util.Scanner;

public class RealtorServiceImpl implements BaseService {
    private static final RealtorDao realtorDao = new RealtorDaoImpl();
    private static final Scanner sc = new Scanner(System.in);
    private final String name = "  Введите имя застройщика: \r\n  > ";
    private final String address = "  Введите адрес застройщика: \r\n  > ";
    private final String year = "  Введите год основания: \r\n  > ";

    @Override
    public void getAll() {
        realtorDao.getAll().forEach(System.out::println);
    }

    @Override
    public void getEntity() {
        System.out.print(name);
        String id = sc.nextLine();
        Realtor realtor = realtorDao.get(new Realtor(id));
        System.out.println(realtor);
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(name);
            String realtorName = sc.nextLine();
            System.out.print(year);
            int foundationYear = sc.nextInt();
            realtorDao.update(new Realtor(realtorName, foundationYear));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void delete() {
        System.out.print(name);
        realtorDao.delete(new Realtor(sc.nextLine()));
    }

    @Override
    public void create() {
        Realtor real = new Realtor();
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(name);
            real.setName(sc.nextLine());
            System.out.print(address);
            real.setAddress(sc.nextLine());
            System.out.print(year);
            real.setFoundationYear(sc.nextInt());
            realtorDao.create(real);
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }
}
