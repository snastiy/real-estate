package service;

import access.HouseDao;
import access.impl.HouseDaoImpl;
import model.House;

import java.util.InputMismatchException;
import java.util.Scanner;

public class HouseServiceImpl implements BaseService{
    private static final HouseDao houseDao = new HouseDaoImpl();
    private static final Scanner sc = new Scanner(System.in);
    private final String entrance = "  Введите кол-во подъездов: \r\n  > ";
    private final String floor = "  Введите кол-во этажей: \r\n  > ";
    private final String address = "  Введите адрес дома: \r\n  > ";
    private final String realtor = "  Введите имя застройщика: \r\n  > ";

    @Override
    public void getAll() {
        houseDao.getAll().forEach(System.out::println);
    }

    @Override
    public void getEntity() {
        System.out.print(address);
        String id = sc.nextLine();
        House house = houseDao.get(new House(id));
        System.out.println(house);
    }

    @Override
    public void delete() {
        System.out.print(address);
        houseDao.delete(new House(sc.nextLine()));
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(address);
            String houseAddress = sc.nextLine();
            System.out.print(realtor);
            String houseRealtor = sc.nextLine();
            houseDao.update(new House(houseAddress, houseRealtor));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void create() {
        House house = new House();
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(address);
            house.setAddress(sc.nextLine());
            System.out.print(realtor);
            house.setRealtorName(sc.nextLine());
            System.out.print(floor);
            house.setFloorNumber(sc.nextInt());
            System.out.print(entrance);
            house.setEntranceNumber(sc.nextInt());
            houseDao.create(house);
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }
}