package service;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ServiceFactory {
    private BaseService baseService;
    private final Scanner sc = new Scanner(System.in);

    public void crud() {
        boolean fl = true;
        int a = 0;
        while(fl) {
            System.out.print(" > ");
            try {
                a = sc.nextInt();
            } catch (InputMismatchException e) {
                System.err.println("Wrong Input Data Type");
            }
            switch(a) {
                case 1 -> baseService.getAll();
                case 2 -> baseService.getEntity();
                case 3 -> baseService.update();
                case 4 -> baseService.create();
                case 5 -> baseService.delete();
                case 6 -> fl = false;
                default -> System.out.println("Input error");
            }
        }
    }

    public void start () {
        boolean fl = true;
        while(fl) {
            System.out.print("Таблицы: 1 - Застройщик; 2 - Владелец; 3 - Дом; 4 - Квартира; 5 - Владеет; 6 - /exit \r\n> ");
            int a = 0;
            try {
                a = sc.nextInt();
            } catch (InputMismatchException e) {
                System.err.println("Wrong Input Data Type");
            }
            System.out.println(" Операции: 1 - Просмотр таблицы; 2 - Выбор записи; 3 - Редактирование; " +
                        "4 - Добавление; 5 - Удаление; 6 - /exit");
            switch(a) {
                case 1 -> baseService = new RealtorServiceImpl();
                case 2 -> baseService = new OwnerServiceImpl();
                case 3 -> baseService = new HouseServiceImpl();
                case 4 -> baseService = new FlatServiceImpl();
                case 5 -> baseService = new OwnershipServiceImpl();
                case 6 -> fl = false;
                default -> System.out.println("Input error");
            }
            crud();
        }
    }
}
