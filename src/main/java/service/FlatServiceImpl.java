package service;

import access.FlatDao;
import access.impl.FlatDaoImpl;
import model.Flat;
import model.FlatType;
import java.util.InputMismatchException;
import java.util.Scanner;

public class FlatServiceImpl implements BaseService{
    private static final FlatDao flatDao = new FlatDaoImpl();
    private static final Scanner sc = new Scanner(System.in);
    private final String entrance = "  Введите номер подъезда: \r\n  > ";
    private final String floor = "  Введите этаж: \r\n  > ";
    private final String address = "  Введите адрес дома: \r\n  > ";
    private final String number = "  Введите номер квартиры: \r\n  > ";
    private final String type = "  Введите тип квартиры(STUDIO, ONE_ROOM, etc): \r\n  > ";
    private final String area = "  Введите площадь квартиры: \r\n  > ";
    private final String price = "  Введите цену квартиры: \r\n  > ";

    @Override
    public void getAll() {
        flatDao.getAll().forEach(System.out::println);
    }

    @Override
    public void getEntity() {
        try {
            System.out.print(number);
            long id = sc.nextLong();
            Flat flat = flatDao.get(new Flat(id));
            System.out.println(flat);
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void delete() {
        try {
            System.out.print(number);
            long id = sc.nextLong();
            Flat flat = flatDao.get(new Flat(id));
            flatDao.delete(flat);
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print(number);
            long id = sc.nextLong();
            flatDao.update(new Flat(id));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void create() {
        Scanner sc = new Scanner(System.in);
        Flat flat = new Flat();
        try {
            System.out.print(address);
            flat.setAddress(sc.nextLine());
            System.out.print(type);
            String flatType = sc.nextLine();
            flat.setFlatType(FlatType.valueOf(flatType));
            System.out.print(entrance);
            flat.setEntrance(sc.nextInt());
            System.out.print(floor);
            flat.setFloor(sc.nextInt());
            System.out.print(area);
            flat.setArea(sc.nextBigDecimal());
            System.out.print(price);
            flat.setPrice(sc.nextBigDecimal());
            flatDao.create(flat);
        } catch (InputMismatchException | IllegalArgumentException e) {
            System.err.println("Wrong Input Data Type");
        }
    }
}
