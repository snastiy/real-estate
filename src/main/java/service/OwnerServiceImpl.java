package service;

import access.OwnerDao;
import access.impl.OwnerDaoImpl;
import model.Owner;

import java.util.InputMismatchException;
import java.util.Scanner;

public class OwnerServiceImpl implements BaseService{
    private static final OwnerDao ownerDao  = new OwnerDaoImpl();
    private static final Scanner sc = new Scanner(System.in);
    private final String fio = "  Введите ФИО владельца: \r\n  > ";
    private final String passport = "  Введите паспорт владельца: \r\n  > ";
    private final String age = "  Введите возраст: \r\n  > ";

    @Override
    public void getAll() {
        ownerDao.getAll().forEach(System.out::println);
    }

    @Override
    public void getEntity() {
        System.out.print(passport);
        String id = sc.nextLine();
        Owner owner = ownerDao.get(new Owner(id));
        System.out.println(owner);
    }

    @Override
    public void delete() {
        System.out.print(passport);
        ownerDao.delete(new Owner(sc.nextLine()));
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(passport);
            String ownerPassport = sc.nextLine();
            System.out.print(age);
            int ownerAge = sc.nextInt();
            ownerDao.update(new Owner(ownerPassport, ownerAge));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void create() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(passport);
            String ownerPassport = sc.nextLine();
            if(ownerPassport.replaceAll("\\s+","").length() != 10 || !ownerPassport.matches("\\d{10}")) {
                throw new InputMismatchException();
            }
            System.out.print(fio);
            String ownerFio = sc.nextLine();
            System.out.print(age);
            int ownerAge = sc.nextInt();
            ownerDao.create(new Owner(ownerFio, ownerPassport, ownerAge));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data");
        }
    }
}