package service;

public interface BaseService {
    void getAll();
    void getEntity();
    void delete();
    void update();
    void create();
}
