package service;

import access.OwnershipDao;
import access.impl.OwnershipDaoImpl;
import model.Ownership;
import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class OwnershipServiceImpl implements BaseService{
    private static final OwnershipDao ownershipDao = new OwnershipDaoImpl();
    private static final Scanner sc = new Scanner(System.in);
    private final String number = "  Введите номер квартиры: \r\n  > ";
    private final String passport = "  Введите паспорт владельца: \r\n  > ";
    private final String share = "  Введите долю: \r\n  > ";

    @Override
    public void getAll() {
        ownershipDao.getAll().forEach(System.out::println);
    }

    @Override
    public void getEntity() {
        try {
            System.out.print(passport);
            String pass = sc.nextLine();
            System.out.print(number);
            long id = sc.nextLong();
            Ownership ownership = ownershipDao.get(new Ownership(pass, id));
            System.out.println(ownership);
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void delete() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(passport);
            String pass = sc.nextLine();

            System.out.print(number);
            long id = sc.nextLong();

            ownershipDao.delete(new Ownership(pass, id));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void update() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print(passport);
            String pass = sc.nextLine();

            System.out.print(number);
            long id = sc.nextLong();

            System.out.print(share);
            BigDecimal flatShare = sc.nextBigDecimal();

            ownershipDao.update(new Ownership(pass, id, flatShare));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data Type");
        }
    }

    @Override
    public void create() {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print(passport);
            String ownerPassport = sc.nextLine();

            if(ownerPassport.replaceAll("\\s+","").length() != 10 || !ownerPassport.matches("\\d{10}")) {
                throw new InputMismatchException();
            }
            System.out.print(number);
            int flatNumber = sc.nextInt();

            System.out.print(share);
            BigDecimal flatShare = sc.nextBigDecimal();

            ownershipDao.create(new Ownership(ownerPassport, flatNumber, flatShare));
        } catch (InputMismatchException e) {
            System.err.println("Wrong Input Data");
        }
    }
}
