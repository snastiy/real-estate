package utils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionManager {
    private static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/real-estate";
    private static final String DATABASE_PASSWORD = "padmin";
    private static final String DATABASE_USERNAME = "postgres";
    private static final String DATABASE_DRIVER = "org.postgresql.Driver";
    private static DataSource dataSource;
    static {
        try {
            Class.forName(DATABASE_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private DBConnectionManager() {}
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
    }
}