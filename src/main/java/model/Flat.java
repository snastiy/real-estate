package model;

import java.math.BigDecimal;

public class Flat {
    private long number;
    private int entrance;
    private int floor;
    private FlatType flatType;
    private BigDecimal area;
    private BigDecimal price;
    private String address;

    public Flat() {}

    public Flat(long number) {
        this.number = number;
    }

    public Flat(long number, int entrance, int floor, FlatType flatType, BigDecimal area, BigDecimal price, String address) {
        this.number = number;
        this.address = address;
        this.entrance = entrance;
        this.flatType = flatType;
        this.floor = floor;
        this.area = area;
        this.price = price;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public void setEntrance(int entrance) {
        this.entrance = entrance;
    }

    public void setFlatType(FlatType flatType) {
        this.flatType = flatType;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public int getEntrance() {
        return entrance;
    }

    public long getNumber() {
        return number;
    }

    public BigDecimal getArea() {
        return area;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public FlatType getFlatType() {
        return flatType;
    }

    public int getFloor() {
        return floor;
    }

    @Override
    public String toString() {
        return "Flat{number=" + number +
                ", address=" + address +
                ", entrance=" + entrance +
                ", flatType=" + flatType +
                ", floor=" + floor +
                ", area=" + area +
                ", price=" + price +
                '}';
    }
}