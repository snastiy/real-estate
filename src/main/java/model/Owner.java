package model;

public class Owner {
    private String fio;
    private String passport;
    private int age;

    public Owner(){}

    public Owner(String passport){
        this.passport = passport;
    }

    public Owner(String passport, int age){
        this.passport = passport;
        this.age = age;
    }

    public Owner(String fio, String passport, int age){
        this.fio = fio;
        this.passport = passport;
        this.age = age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPassport() {
        return passport;
    }

    public String getFio() {
        return fio;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Owner{" +
                "fio='" + fio + '\'' +
                ", passport=" + passport +
                ", age=" + age +
                '}';
    }
}