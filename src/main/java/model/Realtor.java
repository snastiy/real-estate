package model;

public class Realtor {
    private String address;
    private String name;
    private int foundationYear;

    public Realtor() {}

    public Realtor(String name) {
        this.name = name;
    }

    public Realtor(String name, int foundationYear) {
        this.name = name;
        this.foundationYear = foundationYear;
    }

    public Realtor(String address, String name, int foundationYear) {
        this.address = address;
        this.name = name;
        this.foundationYear = foundationYear;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFoundationYear(int foundationYear) {
        this.foundationYear = foundationYear;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getFoundationYear() {
        return foundationYear;
    }

    @Override
    public String toString() {
        return "Realtor{" +
                "address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", foundationYear=" + foundationYear +
                '}';
    }
}