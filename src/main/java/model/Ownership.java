package model;

import java.math.BigDecimal;

public class Ownership {
    private String passport;
    private long flatNumber;
    private BigDecimal share;

    public Ownership() {}

    public Ownership(String passport, long flatNumber) {
        this.passport = passport;
        this.flatNumber = flatNumber;
    }

    public Ownership(String passport, long flatNumber, BigDecimal share) {
        this.passport = passport;
        this.flatNumber = flatNumber;
        this.share = share;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public void setFlatNumber(long flatNumber) {
        this.flatNumber = flatNumber;
    }

    public void setShare(BigDecimal share) {
        this.share = share;
    }

    public long getFlatNumber() {
        return flatNumber;
    }

    public String getPassport() {
        return passport;
    }

    public BigDecimal getShare() {
        return share;
    }

    @Override
    public String toString() {
        return "Ownership{" +
                "passport=" + passport +
                ", flatNumber=" + flatNumber +
                ", share=" + share +
                '}';
    }
}