package model;

public class House {
    private int floorNumber;
    private int entranceNumber;
    private String address;
    private String realtorName;

    public House() {}

    public House(String address) {
        this.address = address;
    }

    public House(String address, String realtorName) {
        this.address = address;
        this.realtorName = realtorName;
    }

    public House(int floorNumber, int entranceNumber, String address, String realtorName) {
        this.floorNumber = floorNumber;
        this.entranceNumber = entranceNumber;
        this.address = address;
        this.realtorName = realtorName;
    }

    public void setEntranceNumber(int entranceNumber) {
        this.entranceNumber = entranceNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRealtorName(String realtorName) {
        this.realtorName = realtorName;
    }

    public int getEntranceNumber() {
        return entranceNumber;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public String getAddress() {
        return address;
    }

    public String getRealtorName() {
        return realtorName;
    }

    @Override
    public String toString() {
        return "House{" +
                "floorNumber=" + floorNumber +
                ", entranceNumber=" + entranceNumber +
                ", address='" + address + '\'' +
                ", realtorName='" + realtorName + '\'' +
                '}';
    }
}