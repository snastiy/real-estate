package access.impl;

import access.FlatDao;
import model.Flat;
import model.FlatType;
import utils.DBConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FlatDaoImpl implements FlatDao {
    private static final String CREATE_QUERY =
            "INSERT INTO FLAT(ENTRANCE, FLOORNUMBER, FLATTYPE, AREA, PRICE, HOUSEADDRESS) VALUES (?, ?, ?, ?, ?, ?)";

    private static final String READ_QUERY =
            "SELECT * FROM FLAT WHERE FLATNUMBER = ?";

    private static final String UPDATE_QUERY =
            "UPDATE FLAT SET PRICE = PRICE + PRICE where FLATNUMBER = ?";

    private static final String REMOVE_QUERY =
            "DELETE FROM FLAT WHERE FLATNUMBER = ?";

    private static final String FIND_ALL_QUERY =
            "SELECT * FROM FLAT ORDER BY FLATNUMBER";


    @Override
    public List<Flat> getAll() {
        List<Flat> flatList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBConnectionManager.getConnection();
            statement = connection.prepareStatement(FIND_ALL_QUERY);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Flat flat = new Flat(
                        resultSet.getInt("FLATNUMBER"),
                        resultSet.getInt("ENTRANCE"),
                        resultSet.getInt("FLOORNUMBER"),
                        FlatType.valueOf(resultSet.getString("FLATTYPE")),
                        resultSet.getBigDecimal("AREA"),
                        resultSet.getBigDecimal("PRICE"),
                        resultSet.getString("HOUSEADDRESS")
                        );
                flatList.add(flat);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
        return flatList;
    }

    @Override
    public void create(Flat flat) {
        ResultSet resultSet = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, flat.getEntrance());
            preparedStatement.setInt(2, flat.getFloor());
            preparedStatement.setString(3, String.valueOf(flat.getFlatType()));
            preparedStatement.setBigDecimal(4, flat.getArea());
            preparedStatement.setBigDecimal(5, flat.getPrice());
            preparedStatement.setString(6, flat.getAddress());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                flat.setNumber(resultSet.getLong(1));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
            close(resultSet);
        }
    }

    @Override
    public Flat get(Flat flat) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Flat flt = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(READ_QUERY);
            preparedStatement.setLong(1, flat.getNumber());
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            flt = new Flat(
                    resultSet.getInt("FLATNUMBER"),
                    resultSet.getInt("ENTRANCE"),
                    resultSet.getInt("FLOORNUMBER"),
                    FlatType.valueOf(resultSet.getString("FLATTYPE")),
                    resultSet.getBigDecimal("AREA"),
                    resultSet.getBigDecimal("PRICE"),
                    resultSet.getString("HOUSEADDRESS")
            );
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
            close(resultSet);
        }
        return flt;
    }

    @Override
    public void update(Flat flat) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setLong(1, flat.getNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public void delete(Flat flat) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(REMOVE_QUERY);
            preparedStatement.setLong(1, flat.getNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}