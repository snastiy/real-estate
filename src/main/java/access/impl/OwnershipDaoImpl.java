package access.impl;

import access.OwnershipDao;
import model.Ownership;
import utils.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OwnershipDaoImpl implements OwnershipDao {
    private static final String CREATE_QUERY =
            "INSERT INTO OWNERSHIP(PASSPORT, FLATNUMBER, FLATSHARE) VALUES (?, ?, ?)";

    private static final String READ_QUERY =
            "SELECT * FROM OWNERSHIP WHERE PASSPORT = ? AND FLATNUMBER = ?";

    private static final String UPDATE_QUERY =
            "UPDATE OWNERSHIP SET FLATSHARE = ? where PASSPORT = ? AND FLATNUMBER = ?";

    private static final String REMOVE_QUERY =
            "DELETE FROM OWNERSHIP WHERE PASSPORT = ? AND FLATNUMBER = ?";

    private static final String FIND_ALL_QUERY =
            "SELECT * FROM OWNERSHIP";

    @Override
    public List<Ownership> getAll() {
        List<Ownership> ownershipList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBConnectionManager.getConnection();
            statement = connection.prepareStatement(FIND_ALL_QUERY);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Ownership ownership = new Ownership(
                        resultSet.getString("PASSPORT"),
                        resultSet.getLong("FLATNUMBER"),
                        resultSet.getBigDecimal("FLATSHARE")
                );
                ownershipList.add(ownership);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
        return ownershipList;
    }

    @Override
    public void create(Ownership ownership) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setString(1, ownership.getPassport());
            preparedStatement.setLong(2, ownership.getFlatNumber());
            preparedStatement.setBigDecimal(3, ownership.getShare());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public Ownership get(Ownership ownership) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Ownership own = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(READ_QUERY);
            preparedStatement.setString(1, ownership.getPassport());
            preparedStatement.setLong(2, ownership.getFlatNumber());
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            own = new Ownership(
                    resultSet.getString("PASSPORT"),
                    resultSet.getLong("FLATNUMBER"),
                    resultSet.getBigDecimal("FLATSHARE")
            );
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
            close(resultSet);
        }
        return own;
    }

    @Override
    public void update(Ownership ownership) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setBigDecimal(1, ownership.getShare());
            preparedStatement.setString(2, ownership.getPassport());
            preparedStatement.setLong(3, ownership.getFlatNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public void delete(Ownership ownership) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(REMOVE_QUERY);
            preparedStatement.setString(1, ownership.getPassport());
            preparedStatement.setLong(2, ownership.getFlatNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}
