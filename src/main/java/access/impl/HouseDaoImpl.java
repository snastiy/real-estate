package access.impl;

import access.HouseDao;
import model.House;
import utils.DBConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HouseDaoImpl implements HouseDao {
    private static final String CREATE_QUERY =
            "INSERT INTO HOUSE(FLOORNUMBER, ENTRANCENUMBER, ADDRESS, REALTORNAME) VALUES ( ?, ?, ?, ?)";

    private static final String READ_QUERY =
            "SELECT * FROM HOUSE WHERE ADDRESS = ?";

    private static final String UPDATE_QUERY =
            "UPDATE HOUSE SET REALTORNAME = ? where ADDRESS = ?";

    private static final String REMOVE_QUERY =
            "DELETE FROM HOUSE WHERE ADDRESS = ?";

    private static final String FIND_ALL_QUERY =
            "SELECT * FROM HOUSE";

    @Override
    public List<House> getAll() {
        List<House> houseList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBConnectionManager.getConnection();
            statement = connection.prepareStatement(FIND_ALL_QUERY);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                House house = new House(
                        resultSet.getInt("floorNumber"),
                        resultSet.getInt("entranceNumber"),
                        resultSet.getString("address"),
                        resultSet.getString("realtorName")
                );
                houseList.add(house);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
        return houseList;
    }

    @Override
    public void create(House house) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setInt(1, house.getFloorNumber());
            preparedStatement.setInt(2, house.getEntranceNumber());
            preparedStatement.setString(3, house.getAddress());
            preparedStatement.setString(4, house.getRealtorName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public House get(House house) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        House dom = new House();
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(READ_QUERY);
            preparedStatement.setString(1, house.getAddress());
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            dom = new House(
                    resultSet.getInt("floorNumber"),
                    resultSet.getInt("entranceNumber"),
                    resultSet.getString("address"),
                    resultSet.getString("realtorName")
            );
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
            close(resultSet);
        }
        return dom;
    }

    @Override
    public void update(House house) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setString(1, house.getRealtorName());
            preparedStatement.setString(2, house.getAddress());
            System.out.println(preparedStatement.executeUpdate());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public void delete(House house) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(REMOVE_QUERY);
            preparedStatement.setString(1, house.getAddress());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}