package access.impl;

import access.OwnerDao;
import model.Owner;
import utils.DBConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OwnerDaoImpl implements OwnerDao {
    private static final String CREATE_QUERY =
            "INSERT INTO LANDLORD(FIO, PASSPORT, AGE) VALUES ( ?, ?, ?)";

    private static final String READ_QUERY =
            "SELECT * FROM LANDLORD WHERE PASSPORT = ?";

    private static final String UPDATE_QUERY =
            "UPDATE LANDLORD SET AGE = ? where PASSPORT = ?";

    private static final String REMOVE_QUERY =
            "DELETE FROM LANDLORD WHERE PASSPORT = ?";

    private static final String FIND_ALL_QUERY =
            "SELECT * FROM LANDLORD";

    @Override
    public List<Owner> getAll() {
        List<Owner> ownerList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBConnectionManager.getConnection();
            statement = connection.prepareStatement(FIND_ALL_QUERY);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Owner owner = new Owner(
                        resultSet.getString("fio"),
                        resultSet.getString("passport"),
                        resultSet.getInt("age")
                );
                ownerList.add(owner);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
        return ownerList;
    }

    @Override
    public void create(Owner owner) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setString(1, owner.getFio());
            preparedStatement.setString(2, owner.getPassport());
            preparedStatement.setInt(3, owner.getAge());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public Owner get(Owner owner) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Owner own = new Owner();
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(READ_QUERY);
            preparedStatement.setString(1, owner.getPassport());
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                own.setFio(resultSet.getString("fio"));
                own.setPassport(resultSet.getString("passport"));
                own.setAge(resultSet.getInt("age"));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
            close(resultSet);
        }
        return own;
    }

    @Override
    public void update(Owner owner) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setInt(1, owner.getAge());
            preparedStatement.setString(2, owner.getPassport());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public void delete(Owner owner) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(REMOVE_QUERY);
            preparedStatement.setString(1, owner.getPassport());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}