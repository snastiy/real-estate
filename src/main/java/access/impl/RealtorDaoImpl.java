package access.impl;

import access.RealtorDao;
import model.Realtor;
import utils.DBConnectionManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RealtorDaoImpl implements RealtorDao {
    private static final String CREATE_QUERY =
            "INSERT INTO REALTOR(REALTORNAME, ADDRESS, FOUNDATIONYEAR) VALUES ( ?, ?, ?)";

    private static final String READ_QUERY =
            "SELECT * FROM REALTOR WHERE REALTORNAME = ?";

    private static final String UPDATE_QUERY =
            "UPDATE REALTOR SET FOUNDATIONYEAR = ? where REALTORNAME = ?";

    private static final String REMOVE_QUERY =
            "DELETE FROM REALTOR WHERE REALTORNAME = ?";

    private static final String FIND_ALL_QUERY =
            "SELECT * FROM REALTOR";

    @Override
    public List<Realtor> getAll() {
        List<Realtor> realtorList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBConnectionManager.getConnection();
            statement = connection.prepareStatement(FIND_ALL_QUERY);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Realtor realtor = new Realtor(
                        resultSet.getString("address"),
                        resultSet.getString("realtorName"),
                        resultSet.getInt("foundationYear")
                );
                realtorList.add(realtor);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(statement);
            close(connection);
            close(resultSet);
        }
        return realtorList;
    }

    @Override
    public void create(Realtor realtor) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setString(1, realtor.getName());
            preparedStatement.setString(2, realtor.getAddress());
            preparedStatement.setInt(3, realtor.getFoundationYear());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public Realtor get(Realtor realtor) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Realtor real = new Realtor();
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(READ_QUERY);
            preparedStatement.setString(1, realtor.getName());
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                real.setFoundationYear(resultSet.getInt("foundationYear"));
                real.setAddress(resultSet.getString("address"));
                real.setName(resultSet.getString("realtorName"));
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
            close(resultSet);
        }
        return real;
    }

    @Override
    public void update(Realtor realtor) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_QUERY);
            preparedStatement.setInt(1, realtor.getFoundationYear());
            preparedStatement.setString(2, realtor.getName());
            System.out.println(preparedStatement.executeUpdate());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }

    @Override
    public void delete(Realtor realtor) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBConnectionManager.getConnection();
            preparedStatement = connection.prepareStatement(REMOVE_QUERY);
            preparedStatement.setString(1, realtor.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            close(preparedStatement);
            close(connection);
        }
    }
}