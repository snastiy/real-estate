import service.ServiceFactory;

public class Main {
    public static void main(String[] args) {
        ServiceFactory serviceFactory = new ServiceFactory();
        serviceFactory.start();
    }
}