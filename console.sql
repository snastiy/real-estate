DROP TABLE OWNERSHIP;
DROP TABLE FLAT;
DROP TABLE HOUSE;
DROP TABLE LANDLORD;
DROP TABLE REALTOR;

CREATE TABLE LANDLORD (
                          fio VARCHAR(50) NOT NULL CHECK (fio <> ''),
                          passport CHAR(10) NOT NULL PRIMARY KEY,
                          age int NOT NULL CHECK (age > 0 AND age <= 115)
);

CREATE TABLE REALTOR (
                         realtorName VARCHAR(50) NOT NULL CHECK (realtorName <> '') PRIMARY KEY,
                         address VARCHAR(50) NOT NULL CHECK (address <> ''),
                         foundationYear INT NOT NULL CHECK (foundationYear > 1800 AND foundationYear < 2022)
);

CREATE TABLE HOUSE (
                       floorNumber INT NOT NULL CHECK (floorNumber > 0),
                       entranceNumber INT NOT NULL CHECK (entranceNumber > 0),
                       address VARCHAR(50) NOT NULL CHECK (address <> '') PRIMARY KEY,
                       realtorName VARCHAR(50) NOT NULL REFERENCES REALTOR(realtorName) ON DELETE RESTRICT
);

DROP TABLE IF EXISTS FLAT;
CREATE TABLE FLAT (
                      flatNumber SMALLSERIAL NOT NULL PRIMARY KEY,
                      entrance INT NOT NULL CHECK (entrance > 0),
                      floorNumber INT NOT NULL CHECK (floorNumber > 0),
                      flatType VARCHAR(50) NOT NULL CHECK (flatType <> ''),
                      area NUMERIC(5,2) NOT NULL,
                      price NUMERIC(9,2) NOT NULL DEFAULT 0,
                      houseAddress VARCHAR(50) NOT NULL REFERENCES HOUSE(address) ON DELETE RESTRICT
);

DROP TABLE IF EXISTS OWNERSHIP;
CREATE TABLE OWNERSHIP (
                      passport CHAR(10) REFERENCES LANDLORD(passport) ON DELETE CASCADE,
                      flatNumber SMALLSERIAL NOT NULL REFERENCES FLAT(flatNumber) ON DELETE CASCADE,
                      flatShare NUMERIC(5,2) NOT NULL CHECK (flatShare <= 100 AND flatShare >= 5),
                      UNIQUE(passport, flatNumber)
);

DROP FUNCTION IF EXISTS check_sum_share();
CREATE FUNCTION check_sum_share()
    RETURNS trigger
    LANGUAGE plpgsql AS
$func$
    DECLARE
        _share INT;
    BEGIN
        SELECT SUM(flatShare)
        FROM OWNERSHIP
        WHERE flatNumber = NEW.flatNumber
        INTO _share;

        IF (_share + NEW.flatShare) > 100 THEN
            RAISE EXCEPTION 'SHARE EXCEEDS 100.';
        ELSE
            RETURN NEW;
        END IF;
    END;
$func$;

SELECT SUM(flatShare)
FROM OWNERSHIP;

DROP TRIGGER IF EXISTS share_trigger ON OWNERSHIP;
CREATE TRIGGER share_trigger
    BEFORE INSERT OR UPDATE ON OWNERSHIP
    FOR EACH ROW
EXECUTE PROCEDURE check_sum_share();

INSERT INTO LANDLORD (fio, passport, age) VALUES('Татьяна Самойлова','0000000000', 34);

INSERT INTO REALTOR (realtorName, address, foundationYear) VALUES('ПИК', 'Москва ул.Пушкина д.Кол', 1989);

INSERT INTO HOUSE (floorNumber, entranceNumber, address, realtorName) VALUES(3, 1, 'ул. Митча д.1', 'ПИК');

INSERT INTO FLAT (entrance, floorNumber, flatType, area, price, houseAddress) VALUES(1, 1, 'ONE_ROOM', 60.0, 1599999.99, 'ул. Митча д.1');

INSERT INTO OWNERSHIP (passport, flatNumber, flatShare) VALUES('0000000000', 1, 90.00);